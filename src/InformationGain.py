import math


class InformationGain:

    @staticmethod
    # calculation of information gain during partitioning "complete data" according to value
    # of "colmn_index" and classification of with respect to enquired column
    def calculate_information_gain(complete_data, column_index, enquired_column):
        information_gain = InformationGain.__calculate_entropy(complete_data, enquired_column)
        data_groups = InformationGain.divide_data_into_groups(complete_data, column_index)
        length_of_complete_data = len(complete_data)
        for _, value in data_groups.items():
            information_gain -= len(value) / length_of_complete_data * InformationGain.__calculate_entropy(value,
                                                                                                           enquired_column)
        return information_gain

    @staticmethod
    # calculation of entropy for data that is classified based on value in column "enquired column"
    def __calculate_entropy(data, enquired_column):
        value_counts = InformationGain.__count_groups(data, enquired_column)
        entropy = 0
        length_of_data = len(data)
        for _, value_i in value_counts.items():
            p_i = value_i / length_of_data
            entropy -= p_i * math.log(p_i, 2)
        return entropy

    @staticmethod
    # grouping and counting elements of completed_data list by the use enquired column
    def __count_groups(data, enquired_column):
        values = {}
        for element in data:
            if values.get(element[enquired_column]) is None:
                values[element[enquired_column]] = 0
            values[element[enquired_column]] += 1
        return values

    @staticmethod
    # dividing data into groups, each group is set down by value of column index (=attribute index)
    def divide_data_into_groups(data, column_index):
        groups_set_down_by_columns = {}
        for data_item in data:
            if groups_set_down_by_columns.get(data_item[column_index]) is None:
                groups_set_down_by_columns[data_item[column_index]] = []
            groups_set_down_by_columns[data_item[column_index]].append(data_item)
        return groups_set_down_by_columns
