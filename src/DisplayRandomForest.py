from anytree import RenderTree, Node
from anytree.exporter import DotExporter


class DisplayRandomForest:

    @staticmethod
    def display_random_forest(forest):
        for i in range(len(forest)):
            print("\nDecision tree number {tree_number} : \n".format(tree_number=i))
            DisplayRandomForest.display_tree(forest[i])

    @staticmethod
    def __display_tree_png(tree, output_file_name):
        DisplayRandomForest.display_tree(tree)
        anytree = DisplayRandomForest.__convert_tree_to_anytree(tree)
        DotExporter(anytree).to_picture(output_file_name)

    @staticmethod
    def display_tree(tree):
        antytree = DisplayRandomForest.__convert_tree_to_anytree(tree)
        for pre, fill, node in RenderTree(antytree):
            print("%s%s" % (pre, node.name))

    @staticmethod
    def __attach_children(parent_node, parent_anytree_node):
        for child_node in parent_node.get_children():
            child_anytree_node = Node(
                child_node.get_node_full_name(), parent=parent_anytree_node)
            DisplayRandomForest.__attach_children(child_node, child_anytree_node)

    @staticmethod
    def __convert_tree_to_anytree(tree):
        anytree = Node("Root")
        DisplayRandomForest.__attach_children(tree, anytree)
        return anytree
