import csv


class DataTreatment:

    @staticmethod
    # method converts data  from csv  to list and divides into attributes presented in random_forest.py
    def order_csv_data(csv_file):
        data_in_list_format = DataTreatment.__convert_csv_to_list_format(csv_file)
        heading = data_in_list_format.pop(0)
        # We assume that in last column of csv file is enquired column
        complete_data = []
        incomplete_data = []
        enquired_column = len(heading) - 1
        [complete_data.append(item) if DataTreatment.__is_complete(item, enquired_column) \
             else incomplete_data.append(item) \
         for item in data_in_list_format]
        return heading, complete_data, incomplete_data, enquired_column

    @staticmethod
    def __convert_csv_to_list_format(csv_file):
        with open(csv_file, 'r') as f:
            csv_object = csv.reader(f)
            data_in_list_format = list(csv_object)
        return data_in_list_format

    @staticmethod
    def __is_complete(item, position):
        return item[position] != '?'
