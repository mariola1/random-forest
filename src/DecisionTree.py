import random

import numpy as np

from src.InformationGain import InformationGain
from src.TreeNode import TreeNode


class DecisionTree:
    @staticmethod
    # Construction of decision tree
    def construct_decision_tree(heading, complete_data, enquired_column):
        available_columns = []
        # set of attributes indices
        length_of_attributes = len(heading)
        [available_columns.append(i) for i in range(0, length_of_attributes) if i != enquired_column]
        tree = TreeNode()
        m = DecisionTree.__choose_m(len(heading))
        DecisionTree.add_children(heading, complete_data, available_columns, enquired_column, tree, m)
        return tree

    @staticmethod
    # construction of random decision tree based on data selected in sample with replacement
    def costruct_random_decision_tree(heading, complete_data, enquired_column):
        sampled_data = DecisionTree.__sample_with_replacement(len(complete_data), complete_data)
        print(sampled_data)
        return DecisionTree.construct_decision_tree(heading, sampled_data, enquired_column)

    @staticmethod
    def add_children(heading, complete_data, available_columns, enquired_column, node, m):
        if not available_columns:
            DecisionTree.__add_leaf(heading, complete_data, enquired_column, node)
            return -1
        selected_attribute = DecisionTree.select_attribute_with_the_biggest_information_gain(available_columns, \
                                                                                             complete_data,
                                                                                             enquired_column, m)
        # dividing data into groups, each group is set down by value of column index (=attribute index)
        data_groups = InformationGain.divide_data_into_groups(complete_data, selected_attribute)
        if len(data_groups.items()) == 1:
            # all the remaining features have the same value so we can take first element from the list
            DecisionTree.__add_leaf(heading, complete_data, enquired_column, node)
            return -1
        # deleting selected attribute from available columns in order to start process of dividing
        # node with the biggest information gain with regard to remaining attributes
        DecisionTree.__delete_used_column(available_columns, selected_attribute)
        # adding child node to the parent node
        for child_group, child_data in data_groups.items():
            child = TreeNode(heading[selected_attribute], child_group)
            # callig for each child node metchod add_children in order to divide futher child node(=attribute with
            # the biggest information gain) with regard to remaining attributes
            DecisionTree.add_children(heading, child_data, list(available_columns), enquired_column, child, m)
            # adding child node to the parent node
            node.add_child(child)

    @staticmethod
    def __delete_used_column(available_columns, selected_column):
        for i in range(len(available_columns)):
            if available_columns[i] == selected_column:
                available_columns.pop(i)
                break

    @staticmethod
    def __add_leaf(heading, complete_data, enquired_column, node):
        leaf_node = TreeNode(heading[enquired_column], complete_data[0][enquired_column])
        TreeNode.add_child(node, leaf_node)

    @staticmethod
    def select_attribute_with_the_biggest_information_gain(available_columns, complete_data, enquired_column, m):
        if len(available_columns) <= m:
            sample_columns = available_columns
        else:
            sample_columns = random.sample(available_columns, m)
        selected_attribute = -1
        information_gain = -1
        for attribute_index in sample_columns:
            sample_information_gain = InformationGain.calculate_information_gain(complete_data, attribute_index, \
                                                                                 enquired_column)
            if sample_information_gain > information_gain:
                information_gain = sample_information_gain
                selected_attribute = attribute_index
        return selected_attribute

    # number of attributes is number of available decision tree variables i.e. properties of one feature
    @staticmethod
    def __choose_m(number_of_attributes):
        m = min(number_of_attributes, 2 * np.sqrt(number_of_attributes))
        return m

    @staticmethod
    # sample with replacement for all accessable data
    def __sample_with_replacement(data_length, input_data):
        sampled_data = []
        [sampled_data.append(random.choice(input_data)) for i in range(data_length)]
        return sampled_data
