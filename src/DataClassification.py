class DataClassification:
    @staticmethod
    def display_classification(random_forest, incomplete_data, heading):
        if not incomplete_data:
            print('There is no data to classification')
            return -1
        else:
            for incomplete_feature in incomplete_data:
                print("\nFeature : {future}\n".format(future=incomplete_feature))
                DataClassification.__display_classification_for_feature(random_forest, incomplete_feature, heading)

    @staticmethod
    def __display_classification_for_feature(random_forest, feature, heading):
        classification = {}
        for i in range(0, len(random_forest)):
            group = DataClassification.__return_classification_of_particular_tree(random_forest[i], heading, feature)
            DataClassification.__dic_inc(classification, group)
            if group is None:
                print('No verdict')
            else:
                print("DT {tree_number} votes on class {class_group} ".format(tree_number=i, class_group=group))
        print(
            "\nThe class with the largest number of votes is {classifier}. Thus the constructed random forest classifies the" \
            "feature {feature} : into the class {classifier}.\n".format(
                classifier=DataClassification.__return_classifier_with_the_highest_number_of_votes(classification), \
                feature=feature))

    @staticmethod
    def __return_classification_of_particular_tree(tree, heading, feature):
        var_to_index = {}
        for index in range(0, len(heading)):
            var_to_index[heading[index]] = index
        return DataClassification.__classify_by_tree(tree, var_to_index, feature)

    @staticmethod
    def __classify_by_tree(tree, var_to_index, feature):
        if tree.is_leaf():
            return tree.get_node_value()
        else:
            for child in tree.get_children():
                if child.is_leaf():
                    return child.get_node_value()
                # checking if value of attribute in node is consistent with value of attribute in incomplete data
                if child.get_node_value() == feature[var_to_index.get(child.get_node_name())]:
                    return DataClassification.__classify_by_tree(child, var_to_index, feature)
            # if dt doesn't contain corresponding values of attributes to feature list function return None value
            return None

    @staticmethod
    def __return_classifier_with_the_highest_number_of_votes(dic):
        key_max_count = None
        for key, value in dic.items():
            if key is not None and (key_max_count is None \
                                    or value > dic[key_max_count]):
                key_max_count = key
        return key_max_count

    @staticmethod
    def __dic_inc(dic, key):
        if key is None:
            pass
        if dic.get(key) is None:
            dic[key] = 1
        else:
            dic[key] += 1
