import sys

from src.DataClassification import DataClassification
from src.DataTreatment import DataTreatment
from src.DecisionTree import DecisionTree
from src.DisplayRandomForest import DisplayRandomForest


def construct_radom_forest(heading, complete_data, enquired_column, tree_count):
    random_forest = []
    for i in range(0, tree_count):
        random_forest.append(DecisionTree.costruct_random_decision_tree(heading, complete_data, enquired_column))
    return random_forest


def run_program():
    if len(sys.argv) < 3:
        sys.exit('Please, input as arguments:\n' +
                 '1. the name of the input CSV file,\n' +
                 '2. the number of the trees in the random forest to be ' +
                 'constructed')

    input_csv_file = sys.argv[1]
    tree_count = int(sys.argv[2])

    # heading - names of columns=names of attributes
    # complete_data - elements of data, for which the value of each attribute is known
    # incompleted_data - elements of data for which the value of decision attribute is unknown
    # enquired column - index of column (counting from 0) in which decision attribute is placed
    (heading, complete_data, incomplete_data, enquired_column) = DataTreatment.order_csv_data(input_csv_file)

    forest = construct_radom_forest(heading, complete_data, enquired_column, tree_count)
    DisplayRandomForest.display_random_forest(forest)
    DataClassification.display_classification(forest, incomplete_data, heading)


if __name__ == "__main__":
    # Program start
    run_program()
