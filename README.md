# Random Forest
Project contains implemented random forest algorithm with visualisation of constructed random trees in this forest.

To run this program there is need to provide:

    1. the name of the input CSV file --> source data
    2. the number of the trees in the random forest to be constructed

